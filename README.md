## Author

Muhammad Lukmanuhakim

## Getting Started

1. Clone from GitLab

```
git clone https://gitlab.com/gakimm/m-employee-management-apps.git
```

2. Install packages

```
yarn install
```

3. Run project

```
yarn start
```

## Authentification
- Username : administrator
- Password : password

## Documentation
I used Mantis Angular Template for templateing : 
[Mantis Angular Documentation](https://codedthemes.gitbook.io/mantis-angular/).

- Bootstrap 5
- Ant Design Icon for Angular
- Angular 18

Angular CLI: 18.0.0-next.3
Node: 22.2.0
Package Manager: yarn 1.22.22
OS: win32 x64

Angular: 18.0.0-next.5
... animations, common, compiler, compiler-cli, core, forms
... platform-browser, platform-browser-dynamic, router

Package                         Version
---------------------------------------------------------
@angular-devkit/architect       0.1800.0-next.3
@angular-devkit/build-angular   18.0.0-next.3
@angular-devkit/core            18.0.0-next.3
@angular-devkit/schematics      18.0.0-next.3
@angular/cdk                    18.0.2
@angular/cli                    18.0.0-next.3
@angular/material               18.0.2
@schematics/angular             18.0.0-next.3
rxjs                            7.8.1
typescript                      5.4.5
zone.js                         0.14.4

## Technology Stack

- Bootstrap 5
- Angular 17
- TypeScript

## Feature

- Login
  - Input Username & Password with validators by angular
  - Using session storage for database
  - Using MatSnackBar for pop up notification
  - Redirect to Dashboard

- Dashboard :
  - Total Summary Employee, Active, Inactive, And On Leave
  - Slicing graph for next implement probably
  
- Employee : 
  - List Of the Employees
    - Search all in tables
    - Sorting No, Date Birth, Status, Group
    - Pagination from 5 to 100
      - prev page, next page, first page, last page
    - I Using Chance.js for generate 100 dummies.
     https://chancejs.com/
    - Edit & Delete only for created manually data.
  - Create Data
    - Using session storage for database temporary
    - Add Data with inteface setting for field
    - Using Services, Route Module, And Share module for any module that can share

- Log Out
  - Click profile on right navbar
  - Select Log out and confirm Yes.
  - You will redirect to login page

## Thank You