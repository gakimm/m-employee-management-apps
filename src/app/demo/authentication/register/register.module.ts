import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import RegisterComponent from './register.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RegisterComponent
  ]
})
export class RegisterModule { }
