// angular import
import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, RouterModule } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SharedModule } from 'src/app/theme/shared/shared.module';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [RouterModule, ReactiveFormsModule, CommonModule, SharedModule],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export default class LoginComponent implements OnInit{
  mainForm: FormGroup;

  constructor(
    private formbuilder: FormBuilder,
    protected mainService: AuthService,
    protected router: Router,
    private snackbar: MatSnackBar
  ){}

  ngOnInit(): void {
    this.setForm();
  }
 
   setForm() {
      this.mainForm = this.formbuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
      });
   }

   onSubmit(): void {
    if (this.mainForm.invalid) {
      console.log(true);
      this.mainForm.markAllAsTouched(); // Mark all fields as touched to show validation errors
      return;
    }
    const dataForm = this.mainForm.value;
    const username = dataForm['username'];
    const password = dataForm['password'];
    
    this.mainService.login(username, password).subscribe(
      (res) => {
        console.log(res);
        // Jika login berhasil
        if(res) {
          this.snackbar.open('Login Successfully!', 'Close', {
            duration: 500,
          }).afterDismissed().subscribe(() => {
            this.router.navigate(['/dashboard']);
          });
        }else {
          this.snackbar.open('Username or Password is wrong !', 'Close', {
            duration: 3000,
          });
          return
        }
      },
      (err) => {
        // Jika terjadi error saat login
        this.snackbar.open('Something Wrong', 'Close', {
          duration: 3000,
        });
      }
    );
  }

  logout() {
    this.mainService.logout();
    this.router.navigate(['/login']);
  }

  // public method
  SignInOptions = [
    {
      image: 'assets/images/authentication/google.svg',
      name: 'Google'
    },
    {
      image: 'assets/images/authentication/twitter.svg',
      name: 'Twitter'
    },
    {
      image: 'assets/images/authentication/facebook.svg',
      name: 'Facebook'
    }
  ];
  
}
