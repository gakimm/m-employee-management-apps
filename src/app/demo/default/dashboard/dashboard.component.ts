// angular import
import { AfterViewInit, Component } from '@angular/core';
import { CommonModule } from '@angular/common';

// project import
import tableData from 'src/fake-data/default-data.json';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { MonthlyBarChartComponent } from './monthly-bar-chart/monthly-bar-chart.component';
import { IncomeOverviewChartComponent } from './income-overview-chart/income-overview-chart.component';

// icons
import { IconService } from '@ant-design/icons-angular';
import { FallOutline, GiftOutline, MessageOutline, RiseOutline, SettingOutline } from '@ant-design/icons-angular/icons';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-default',
  standalone: true,
  imports: [
    CommonModule,
    SharedModule,
    MonthlyBarChartComponent,
    IncomeOverviewChartComponent
  ],
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DefaultComponent{
  AnalyticEcommerce: any;
  totalEmployee: number = 0;
  percentageEmployee: number;

  totalActive: number = 0;
  percentageActive: number;

  totalInactive: number = 0;
  percentageInactive: number;

  totalOnLeave: number = 0;
  percentageOnLeave: number;

  // constructor
  constructor(
    private iconService: IconService,
    private employeeService: EmployeeService
  ) {
    this.iconService.addIcon(...[RiseOutline, FallOutline, SettingOutline, GiftOutline, MessageOutline]);
    this.fetchData();
  }

 ngOnInit() :void {
  this.fetchData();
  this.AnalyticEcommerce = [
    {
      title: 'Employees Total ',
      amount: this.totalEmployee,
      background: 'bg-light-primary ',
      border: 'border-primary',
      icon: 'rise',
      percentage: this.percentageEmployee + '%',
      color: 'text-primary',
      number: this.totalEmployee
    },
    {
      title: 'Total Active',
      amount: this.totalActive,
      background: 'bg-light-success ',
      border: 'border-success',
      icon: 'rise',
      percentage: this.percentageActive + '%',
      color: 'text-success',
      number: this.totalActive
    },
    {
      title: 'Total Inactive',
      amount: this.totalInactive,
      background: 'bg-light-secondary ',
      border: 'border-secondary',
      icon: 'fall',
      percentage: this.percentageInactive + '%',
      color: 'text-secondary',
      number: this.totalInactive
    },
    {
      title: 'Total On Leave',
      amount: this.totalOnLeave,
      background: 'bg-light-danger ',
      border: 'border-danger',
      icon: 'rise',
      percentage: this.percentageOnLeave + '%',
      color: 'text-danger',
      number: this.totalOnLeave
    }
  ];
 }

  fetchData() {
    const allEmployees = this.employeeService.getAllEmployees();
    // Calculate total employees
    this.totalEmployee = allEmployees.length;
    this.percentageEmployee = this.calculatePercentage(this.totalActive + this.totalInactive + this.totalOnLeave, this.totalEmployee);
    // Calculate total Active
    this.totalActive = allEmployees.filter(emp => emp.status === 'Active').length;
    this.percentageActive = this.calculatePercentage(this.totalActive, this.totalEmployee);
    // Calculate total employees Inactive
    this.totalInactive = allEmployees.filter(emp => emp.status === 'Inactive').length;
    this.percentageInactive = this.calculatePercentage(this.totalInactive, this.totalEmployee);
    // Calculate total employees on leave
    this.totalOnLeave = allEmployees.filter(emp => emp.status === 'On Leave').length;
    this.percentageOnLeave = this.calculatePercentage(this.totalOnLeave, this.totalEmployee);
  }

  calculatePercentage(value: number, total: number): number {
    return total > 0 ? (value / total) * 100 : 0;
  }

}
