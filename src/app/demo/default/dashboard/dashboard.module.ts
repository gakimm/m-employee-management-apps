import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { IncomeOverviewChartComponent } from './income-overview-chart/income-overview-chart.component';
import { MonthlyBarChartComponent } from './monthly-bar-chart/monthly-bar-chart.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    IncomeOverviewChartComponent,
    MonthlyBarChartComponent
  ]
})
export class DashboardModule { }
