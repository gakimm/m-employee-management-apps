import { AfterViewInit, Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { BreadcrumbComponent } from 'src/app/theme/shared/components/breadcrumb/breadcrumb.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.scss'
})
export class DetailComponent implements AfterViewInit {
  mainForm: FormGroup;
  employee: any;
  id: number;
  description: string;
  constructor(
    private formbuilder: FormBuilder,
    protected mainService: EmployeeService,
    protected router: Router,
    private snackbar: MatSnackBar,
    private activatedRoute: ActivatedRoute
  ){
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  ngOnInit(): void {
    this.setForm();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.fetchData();
    });
    
  }

  setForm() {
    this.mainForm = this.formbuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: [this.getDefaultDate(), Validators.required],
      basicSalary: [null],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: [''],
      non_generate: [1]
    });
  }

  getDefaultDate(): string {
    const date = new Date();
    date.setFullYear(2001, 0, 1); // Set to January 1, 2001
    return date.toISOString().substring(0, 10);
  }

  onSubmit(): void {
    if (this.mainForm.invalid) {
      console.log(true);
      this.mainForm.markAllAsTouched(); // Mark all fields as touched to show validation errors
      return;
    }

    const dataForm = this.mainForm.value;
    
    this.mainService.updateEmployee(Number(this.id),dataForm);  
    this.snackbar.open('Data has been saved successfully!', 'Close', {
      duration: 3000,
    }).afterDismissed().subscribe(() => {
      this.router.navigate(['/master/employee']);
    });
  }

  fetchData() {
    const employee = this.mainService.getEmployee(Number(this.id));

    if(employee) {
      this.employee = employee;
      this.description = employee.description;
      this.mainForm.patchValue({
        firstName: employee.firstName,
        lastName: employee.lastName,
        username: employee.username,
        email: employee.email,
        birthDate: employee.birthDate,
        basicSalary: employee.basicSalary,
        status: employee.status,
        group: employee.group,
        description: employee.description,
      });
    }
  }

}
