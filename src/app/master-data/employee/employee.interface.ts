export interface Employee {
  id: number;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date; // Menggunakan tipe Date untuk tanggal lahir
  basicSalary: number; // Menggunakan tipe number untuk gaji dasar (jika bilangan bulat atau desimal)
  status: string;
  group: string;
  description: any;
  non_generate?: number; // 1 => hasil dari input data, null / others => bukan hasil input data
}
