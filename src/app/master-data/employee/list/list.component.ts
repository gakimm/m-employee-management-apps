import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Employee } from '../employee.interface';
import { EmployeeService } from 'src/app/services/employee.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
// icons
import { IconService } from '@ant-design/icons-angular';
import { MenuUnfoldOutline, MenuFoldOutline, SearchOutline, EditOutline, DeleteOutline } from '@ant-design/icons-angular/icons';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements AfterViewInit {

  employees: Employee[] = [];

  displayedColumns: string[] = ['id', 'username', 'firstName', 'lastName', 'email', 'birthDate', 'basicSalary', 'status', 'group', 'action'];
  dataSource = new MatTableDataSource<Employee>(this.employees);

  filterValue: string = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private employeeService: EmployeeService,
    private iconService: IconService,
    private snackbar: MatSnackBar
  ) { 
    this.iconService.addIcon(...[EditOutline, DeleteOutline]);
   }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.loadEmployees();
  }

  loadEmployees(): void {
    this.employees = this.employeeService.getAllEmployees();
    // console.log(this.employees);
    this.dataSource.data = this.employees;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value.trim().toLowerCase();
    this.dataSource.filter = filterValue;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  deleteEmployee(id: number): void {
    if (confirm('Are you sure you want to delete this employee?')) {
      this.employeeService.deleteEmployee(id);
      this.snackbar.open('Data has been deleted!', 'Close', {
        duration: 3000,
      }).afterDismissed().subscribe(() => {
        this.loadEmployees();
      });
    }
  }
}
