import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { CreateComponent } from './create/create.component';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { AuthGuard } from 'src/app/auth.guard';

const routes: Routes = [
  { 
    path: '', 
    redirectTo: 'list', 
    pathMatch: 'full'
  },
  { 
    path: 'list', 
    component: ListComponent
  },
  { 
    path: 'create',
    component: CreateComponent
  },
  { 
    path: 'detail/:id', 
    component: DetailComponent 
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: 
  [
    RouterModule,
    SharedModule
  ]
})
export class EmployeeRoutingModule { }
