import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { BreadcrumbComponent } from 'src/app/theme/shared/components/breadcrumb/breadcrumb.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrl: './create.component.scss'
})
export class CreateComponent {
  mainForm: FormGroup;

  constructor(
    private formbuilder: FormBuilder,
    protected mainService: EmployeeService,
    protected router: Router,
    private snackbar: MatSnackBar
  ){}

  ngOnInit(): void {
   this.setForm();
  }

  setForm() {
    this.mainForm = this.formbuilder.group({
      id: [null],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      birthDate: [this.getDefaultDate(), Validators.required],
      basicSalary: [null],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: [''],
      non_generate: [1],
    });
  }

  getDefaultDate(): string {
    const date = new Date();
    date.setFullYear(2001, 0, 1); // Set to January 1, 2001
    return date.toISOString().substring(0, 10);
  }

  onSubmit(): void {
    if (this.mainForm.invalid) {
      console.log(true);
      this.mainForm.markAllAsTouched(); // Mark all fields as touched to show validation errors
      return;
    }

    this.mainService.saveEmployee(this.mainForm.value);
    this.snackbar.open('Data has been saved successfully!', 'Close', {
      duration: 3000,
    }).afterDismissed().subscribe(() => {
      this.router.navigate(['/master/employee']);
    });
  }
}
