// angular import
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Project import
import { AdminComponent } from './theme/layouts/admin-layout/admin-layout.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  {
    path: 'register',
    loadComponent: () => import('./demo/authentication/register/register.component')
  },
  {
    path: 'login',
    loadComponent: () => import('./demo/authentication/login/login.component')
  },
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        loadComponent: () => import('./demo/default/dashboard/dashboard.component').then((c) => c.DefaultComponent)
      },
      {
        path: 'master/employee',
        loadChildren: () => import('./master-data/employee/employee.module').then((m) => m.EmployeeModule)
      },
      {
        path: '**',
        loadComponent: () => import('./theme/layouts/not-found/not-found.component').then((c) => c.NotFoundComponent)
      },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
