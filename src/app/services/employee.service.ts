import { Injectable } from '@angular/core';
import { Employee } from '../master-data/employee/employee.interface';
import * as Chance from 'chance';

const chance = new Chance();

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  
  private sessionKey = 'employeeForm';
  private fakeEmployees: Employee[] = this.generateFakeEmployees();

  constructor() { }

  generateFakeEmployees(): Employee[] {
    const employees: Employee[] = [];

    for (let i = 1; i <= 100; i++) {
      const employee: Employee = {
        id: i,
        username: chance.twitter(),
        firstName: chance.first(),
        lastName: chance.last(),
        email: chance.email(),
        birthDate: chance.birthday({ string: true }),
        basicSalary: chance.integer({ min: 4500000, max: 10000000 }),
        status: chance.pickone(['Active', 'Inactive', 'On Leave']),
        group: chance.pickone(['IT', 'Finance', 'Admin']),
        description: chance.sentence(),
        non_generate: 0
      };

      employees.push(employee);
    }

    return employees;
  }

  getSavedEmployees(): Employee[] {
    const savedEmployees = sessionStorage.getItem(this.sessionKey);
    return savedEmployees ? JSON.parse(savedEmployees) : [];
  }

  saveEmployee(employee: Employee): void {
    const savedEmployees = this.getSavedEmployees();
    employee.id = this.fakeEmployees.length + savedEmployees.length + 1; // Assign new ID
    savedEmployees.push(employee);
    sessionStorage.setItem(this.sessionKey, JSON.stringify(savedEmployees));
  }

  updateEmployee(id: number, data): void {
    let savedEmployees = this.getSavedEmployees()
    let employeeExists = false;

    // Cari apakah employee dengan ID yang sama sudah ada
    for (let i = 0; i < savedEmployees.length; i++) {
      if (savedEmployees[i].id === id) {
        // Jika employee dengan ID yang sama ditemukan, edit data tersebut
        
        savedEmployees[i] = data;
        savedEmployees[i].id = id;
        employeeExists = true;
        break;
      }
    }

    if (!employeeExists) {
      // Jika employee dengan ID tersebut belum ada, tambahkan ke array
      savedEmployees.push(data);
    }

    // Simpan kembali data ke session storage
    sessionStorage.setItem(this.sessionKey, JSON.stringify(savedEmployees));
  }

  getAllEmployees(): Employee[] {
    const savedEmployees = this.getSavedEmployees();
    const allEmployees = [...this.fakeEmployees, ...savedEmployees];
    return allEmployees.sort((a, b) => b.id - a.id); // Urutkan dari ID terbaru
  }

   getEmployee(id: number): Employee | null {
    const allEmployees = this.getAllEmployees();
    return allEmployees.find(emp => emp.id === id) || null;
  }

  deleteEmployee(id: number): void {
    let allEmployees = this.getSavedEmployees();
    allEmployees = allEmployees.filter(emp => emp.id !== id);
    sessionStorage.setItem(this.sessionKey, JSON.stringify(allEmployees));
  }
}
