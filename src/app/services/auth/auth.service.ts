import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(false);
  private sessionKey = 'isLoggedIn';

  constructor() {
    // Cek apakah pengguna sudah login berdasarkan session storage saat inisialisasi service
    this.loggedIn.next(this.isLoggedIn());
  }

  login(username: string, password: string): Observable<boolean> {
    if (username === 'administrator' && password === 'password') {
      sessionStorage.setItem(this.sessionKey, 'true'); // Simpan status login di session storage
      this.loggedIn.next(true);
      return of(true); // Mengembalikan observable yang mengirimkan nilai true
    } else {
      return of(false); // Mengembalikan observable yang mengirimkan nilai false
    }
  }

  logout(): void {
    sessionStorage.removeItem(this.sessionKey); // Hapus status login dari session storage
    sessionStorage.removeItem('employeeForm');
    this.loggedIn.next(false);
  }

  isLoggedIn(): boolean {
    const isLoggedIn = sessionStorage.getItem(this.sessionKey);
    return isLoggedIn ? JSON.parse(isLoggedIn) : false;
  }

  isLoggedInObservable(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }
}
